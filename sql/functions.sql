/*function selecting stations in a given poi*/
--drop function poi_stations (integer)
create or replace function poi_stations(customer integer, date_start bigint, date_end bigint)
    returns varchar
    language plpgsql
as
$$
declare
    tabName varchar;
begin

    tabName = 'poi_stations_' ||  customer ||'_'|| date_start ||'_'|| date_end;

    execute 'create table if not exists poi_work.poi_stations_' ||  customer ||'_'|| date_start ||'_'|| date_end || ' as
        select
			pp.id,
            pp.name,
            pp.geom geom_poi,
            pp.customer,
            s.commissioning,
            s.id_hex station,
            s.monarch,
            s.geom geom_station
        from
            station_world as s,
            poi_poi as pp
        where pp.customer = $1
        and st_contains(pp.geom, s.geom)'
        using customer;

    return tabName;
end
$$;

--drop function poi_messages (integer, bigint, bigint)
create or replace function poi_messages(customer integer, date_start bigint, date_end bigint)
    returns varchar
    language plpgsql
as
$$
declare
    tabName       varchar;
    station_table varchar;
begin

    tabName = 'poi_messages_' || customer || '_' || date_start || '_' || date_end;

    select poi_stations(customer, date_start, date_end) into station_table;

    execute 'create table if not exists poi_work.' || quote_ident(tabName) || ' as
        select
            row_number() over (partition by device, seqnb order by rank) as rn,
            device,
            time,
            seqnb,
            rank,
            t0.id,
            t0.name,
            t0.geom_poi,
            rssi,
            frames,
            redundancy,
            t0.commissioning,
            t0.station,
            t0.monarch,
            t0.geom_station
        from
            poi_work.' || quote_ident(station_table) || ' as t0
        left join
            poi_messages as pm on (pm.customer = t0.customer and pm.station = t0.station)
        where time > $1
          and time < $2
        order by
            device,
            seqnb'
        using date_start, date_end;

    return tabName;
end
$$;

--drop function poi_best_messages (integer, bigint, bigint)
create or replace function poi_best_messages(customer integer, date_start bigint, date_end bigint)
    returns varchar
    language plpgsql
as
$$
declare
    tabName        varchar;
    messages_table varchar;
begin

    tabName = 'poi_best_messages_' || customer || '_' || date_start || '_' || date_end;

    select poi_messages(customer, date_start, date_end) into messages_table;

    execute 'create table if not exists poi_work.' || quote_ident(tabName) || ' as
        select
            device,
            time,
            seqnb,
            rssi,
            frames,
            redundancy,
            commissioning,
            monarch,
            station,
			geom_station,
			geom_poi,
            lead(seqnb) over win,
            abs(
				case
					when coalesce((seqnb - (lead(seqnb) over win)) + 1, 0) <= 0
						then coalesce((seqnb - (lead(seqnb) over win)) + 1, 0)
					else (seqnb - (lead(seqnb) over win) + 4096) + 1
				end
			) as shift,
            rank,
            id,
            name
        from poi_work.' || quote_ident(messages_table) || '
        where
            rn = 1
        window win as (partition by device order by seqnb)';

    return tabName;
end
$$;

--drop function calculate_kpi_device (integer, bigint, bigint)
create or replace function calculate_kpi_device(customer integer, date_start bigint, date_end bigint)
    returns table
		(
			device               text,
			messages_volume      bigint,
			rssi_average         numeric,
			rssi_std             numeric,
			frame_volume_average numeric,
			redundancy_average   numeric,
			msr                  double precision,
			name                 text,
			id                   integer
		)
    language plpgsql
as
$$
declare
    best_messages_table varchar;
    q                   text;
begin

    select poi_best_messages(customer, date_start, date_end) into best_messages_table;

    q := 'select
            device,
            count(distinct(time)) messages_volume,
            avg(rssi) rssi_average,
            stddev_pop(rssi) rssi_std,
            avg(frames) frame_volume_average,
            avg(redundancy) redundancy_average,
            (count(*)::double precision/(count(*)::double precision+sum(shift)::double precision)) as "msr",
            name,
			id
        from poi_work.' || quote_ident(best_messages_table) || '
        group by
            device,
            name,
			id
        order by messages_volume desc';
		
    return query execute q;
end
$$;

--drop function calculate_kpi_station (integer, bigint, bigint)
create or replace function calculate_kpi_station(customer integer, date_start bigint, date_end bigint)
    returns table
		(
			ratio_msg_recvd      double precision,
			devices_volume       bigint,
			messages_volume      bigint,
			rssi_average         numeric,
			rssi_std             numeric,
			rank_average         numeric,
			rank_std             numeric,
			frame_volume_average numeric,
			redundancy_average   numeric,
			station              text,
			poi                  text,
			poi_id               integer,
			commissioning_date   numeric,
			geom                 text,
			monarch              text
		)
    language plpgsql
as
$$
declare
    stations_table      varchar;
    best_messages_table varchar;
    messages_table      varchar;
    q                   text;
    num_best_messages   bigint;
begin
    stations_table := 'poi_stations_' ||  customer ||'_'|| date_start ||'_'|| date_end;
    select poi_messages(customer, date_start, date_end) into messages_table;
    select poi_best_messages(customer, date_start, date_end) into best_messages_table;

    execute 'select count(*) from poi_work.' || best_messages_table into num_best_messages;

    q := 'with t as (
	select
		count(distinct(device,time))::double precision/' || num_best_messages || '::double precision ratio_msg_recvd,
		count(distinct device) devices_volume,
		count(distinct(device,time)) messages_volume,
		avg(rssi) rssi_average,
		stddev_pop(rssi) rssi_std,
		avg(rank) rank_average,
		stddev_pop(rank) rank_std,
		avg(frames) frame_volume_average,
		avg(redundancy) redundancy_average,
		station,
		name as poi,
		id as poi_id,
		commissioning commissioning_date,
		st_asgeojson(geom_station) as geom,
		monarch
	from poi_work.' || quote_ident(messages_table) || '
	group by
		station,
		name,
		id,
		geom,
		commissioning,
		monarch
	)
	select 
		coalesce(ratio_msg_recvd,ratio_msg_recvd,0) ratio_msg_recvd,
		coalesce(devices_volume,devices_volume,0) devices_volume,
		coalesce(messages_volume,messages_volume,0) messages_volume,
		coalesce(rssi_average,rssi_average,0) rssi_average,
		coalesce(rssi_std,rssi_std,0) rssi_std,
		coalesce(rank_average,rank_average,0) rank_average,
		coalesce(rank_std,rank_std,0) rank_std,
		coalesce(frame_volume_average,frame_volume_average,0) frame_volume_average,
		coalesce(redundancy_average,redundancy_average,0) redundancy_average,
		coalesce(t.station,t.station,b.station) station,
		coalesce(poi,poi,b.name) poi,
		coalesce(poi_id,poi_id,b.id) poi_id,
		coalesce(commissioning_date,commissioning_date,b.commissioning) commissioning_date,
		coalesce(geom,geom, st_asgeojson(geom_station)) geom,
		coalesce(t.monarch, t.monarch,b.monarch) monarch
	from t right join poi_work.' || quote_ident(stations_table) || ' b on (t.station=b.station)';

    return query execute q;
end
$$;

--drop function calculate_kpi_poi (integer, bigint, bigint)
create or replace function calculate_kpi_poi(customer integer, date_start bigint, date_end bigint)
    returns table
		(
			id integer,
			poi text,
			station_transiting_volume bigint,
			station_untransiting_volume bigint,
			monarch bigint,
			devices_volume numeric,
			messages_volume numeric,
			rssi_average numeric,
			rssi_std numeric,
			frame_volume_average numeric,
			redundancy_average numeric,
			avg_stations_msr double precision,
			global_poi_msr double precision,
			geom text
        )
    language plpgsql
as
$$
declare
    stations_table      varchar;
    best_messages_table varchar;
    messages_table      varchar;
    q                   text;
begin
    stations_table := 'poi_stations_' ||  customer ||'_'|| date_start ||'_'|| date_end;
    select poi_messages(customer, date_start, date_end) into messages_table;
    select poi_best_messages(customer, date_start, date_end) into best_messages_table;

    q:= 'with station_stats as (
        select
            station,
            count(distinct device) devices_volume,
            count(distinct(device, time)) messages_volume,
            avg(rssi) rssi_average,
            stddev_pop(rssi) rssi_std,
            avg(frames) frame_volume_average,
            avg(redundancy) redundancy_average,
			id,
            name poi
        from
            poi_work.' || quote_ident(messages_table) || '
        group by
			id,
            name,
            station
    ),
    msr_stats as (
        select
            station,
            (count(*)::double precision/(count(*)::double precision+sum(shift)::double precision)) msr
        from
            poi_work.' || quote_ident(best_messages_table) || '
        group by
            station
    ),
    agg_stats as (
        select
            ss.station,
            devices_volume,
            messages_volume,
            rssi_average,
            rssi_std,
            frame_volume_average,
            redundancy_average,
            id,
            poi,
            msr
        from
            station_stats ss
            join msr_stats ms
            on ss.station=ms.station
    ),
    msr_poi as (
        select
			id,
            name poi,
            (count(*)::double precision/(count(*)::double precision+sum(shift)::double precision)) msr
        from
            poi_work.' || quote_ident(best_messages_table) || '
        group by
            name,
			id
    ),
    enum_stations as (
        select
			id,
            name poi,
            count(station) total_stations,
            sum(case when monarch = ''True'' then 1 else 0 end) monarch,
            st_asgeojson(geom_poi) geom
        from poi_work.' || quote_ident(stations_table) || '
        group by 
			name,
			id,
			geom_poi
    ),
    stats_poi as (
        select
            count(station) station_transiting_volume,
            sum(devices_volume) devices_volume,
            sum(messages_volume) messages_volume,
            avg(rssi_average) rssi_average,
            avg(rssi_std) rssi_std,
            avg(frame_volume_average) frame_volume_average,
            avg(redundancy_average) redundancy_average,
            avg(ag.msr) avg_stations_msr,
            mp.msr global_poi_msr,
            ag.id,
            ag.poi
        from agg_stats ag join msr_poi mp on ag.id=mp.id
        group by
            ag.id,
            ag.poi,
            mp.msr
    )
    select
        es.id,
        es.poi,
        station_transiting_volume,
        (total_stations - station_transiting_volume) station_untransiting_volume,
        monarch,
        devices_volume,
        messages_volume,
        rssi_average,
        rssi_std,
        frame_volume_average,
        redundancy_average,
        avg_stations_msr,
        global_poi_msr,
        geom
    from enum_stations es join stats_poi sp on es.id=sp.id';

    return query execute q;
end
$$;

--drop function clean_kpi_tables (integer)
create or replace function clean_kpi_tables(customer integer, date_start bigint, date_end bigint)
    returns varchar
    language plpgsql
as
$$
begin
    execute 'drop table if exists poi_work.poi_stations_' ||  customer ||'_'|| date_start ||'_'|| date_end;
    execute 'drop table if exists poi_work.poi_messages_'|| customer ||'_'|| date_start ||'_'|| date_end;
    execute 'drop table if exists poi_work.poi_best_messages_'|| customer ||'_'|| date_start ||'_'|| date_end;
    return 'done';
end
$$;
