-- SCHEMA: poi_work

-- DROP SCHEMA poi_work ;

CREATE SCHEMA poi_work AUTHORIZATION postgis_admin;

COMMENT ON SCHEMA poi_work IS 'schema for temporary tables for poi';

GRANT ALL ON SCHEMA poi_work TO PUBLIC;

GRANT ALL ON SCHEMA poi_work TO postgis_admin;