sql_fragment_agg_messages = """
WITH t as (
        SELECT
            ROW_NUMBER() over (partition by device, seqnb order by rank) as rn,
            device,
            time,
            seqnb,
            rank,
            pp.name,
            pp.geom geom_poi,
            rssi,
            frames,
            redundancy,
            s.commissioning,
            s.monarch,
			s.id_hex station,
			s.geom geom_station
        FROM
            poi_messages as pm,
            station_world as s,
            poi_poi as pp
        WHERE
            pm.station = s.id_hex
            AND pm.customer = pp.customer
            AND pp.customer = '{0}'
            AND ST_CONTAINS(pp.geom, s.geom)
            AND time>{1}
            AND time<{2}
        ORDER by
            device,
            seqnb
    ),
    t2 as (
        SELECT
            device,
            time,
            seqnb,
            rssi,
            frames,
            redundancy,
            commissioning,
            monarch,
            station,
			geom_station,
			geom_poi,
            lead(seqnb) over win,
            abs(
            case
                when coalesce((seqnb - (lead(seqnb) over win)) + 1, 0) <= 0
                    then coalesce((seqnb - (lead(seqnb) over win)) + 1, 0)
                else (seqnb - (lead(seqnb) over win) + 4096) + 1
            end) as shift,
            rank,
            name
        FROM
            t
        WHERE
            rn = 1
        WINDOW win as (partition by device order by seqnb)
    )"""



  calculate_kpi_pois =WITH t as (
        SELECT
            sum(case when monarch = 'True' then 1 else 0 end) monarch,
            pp.name,
            count(id_hex) as st
        FROM
            station_world as s,
            poi_poi as pp
        WHERE
            ST_CONTAINS(pp.geom, s.geom)
        GROUP by
            pp.name
        )
        SELECT
            t.st as station_transiting_volume,
            count(DISTINCT device) devices_volume,
            count(DISTINCT(device, time)) messages_volume,
            case when count(DISTINCT(device, time)) > 0 then 'poi with data' else 'poi without data' end as status,
            avg(rssi) rssi_average,
            stddev_pop(rssi) rssi_std,
            avg(rank) avg_rank,
            avg(frames) frame_volume_average,
            avg(redundancy) redundancy_average,
            pp.name as poi,
            t.monarch station_transiting_ok_monarch_volume,
            ST_AsGeoJSON(pp.geom) geom
        FROM
            poi_messages as pm,
            station_world as s,
            poi_poi as pp,
            t
        WHERE
            pm.station = s.id_hex
            AND pp.name = t.name
            AND pm.customer = pp.customer
            AND pp.customer = '{0}'
            AND ST_CONTAINS(pp.geom, s.geom)
            AND time>{1}
            AND time<{2}
        GROUP by
            pp.name,
            t.monarch,
            t.st,
            pp.geom
        order by messages desc;""".format(cid, start, stop)


calculate_kpi_device =     SELECT
        device,
        count(DISTINCT(time)) messages_volume,
        avg(rssi) rssi_average,
        stddev_pop(rssi) rssi_std,
        avg(frames) frame_volume_average,
        avg(redundancy) redundancy_average,
        (count(*)::double precision/(count(*)::double precision+sum(shift)::double precision)) as "MSR",
        name
    FROM
        t2
    GROUP by
        device,
        name
    order by messages_volume desc;

  calculate_kpi_station = SELECT
        count(DISTINCT device) devices_volume_rank1,
        count(DISTINCT(device,seqnb)) messages_volume_rank1,
        avg(rssi) rssi_average_rank1,
        stddev_pop(rssi) rssi_std_rank1,
        avg(rank) rank_average,
        avg(frames) frame_volume_average_rank1,
        avg(redundancy) redundancy_average_rank1,
        (count(*)::double precision/(count(*)::double precision+sum(shift)::double precision)) as "MSR",
        station,
        name as poi,
        commissioning commissioning_date,
        ST_AsGeoJSON(geom_station) as geom,
        monarch
    FROM
        t2
    GROUP by
        station,
        name,
        geom,
        commissioning,
        monarch



last_sql = t0, t1, t2,
WITH t0 as(
SELECT
		pp.name,
		pp.geom geom_poi,
		pp.customer,
		s.commissioning,
		s.id_hex station,
		s.monarch,
		s.geom geom_station
	FROM
		station_world as s,
		poi_poi as pp
	WHERE
		pp.customer = '2'
		AND ST_CONTAINS(pp.geom, s.geom)
),
t1 as (
	SELECT
		ROW_NUMBER() over (partition by device, seqnb order by rank) as rn,
		device,
		time,
		seqnb,
		rank,
		t0.name,
		t0.geom_poi,
		rssi,
		frames,
		redundancy,
		t0.commissioning,
		t0.station,
		t0.monarch,
		t0.geom_station
	FROM
		t0
		LEFT JOIN
		poi_messages as pm ON (pm.customer = t0.customer AND pm.station = t0.station)
	WHERE
		time>1603231200
		AND time<1603317600
	ORDER by
		device,
		seqnb
),
t2 as (
	select
		t1.*,
		t0.station
	from t1 right join t0 on (t1.station = t0.station)
)select * from t2 where device is null
,
t2 as (
	SELECT
		device,
		time,
		seqnb,
		rssi,
		frames,
		redundancy,
		commissioning,
		station,
		monarch,
		geom_station,
		geom_poi,
		rn,
		lead(seqnb) over win,
		abs(
		case
			when coalesce((seqnb - (lead(seqnb) over win)) + 1, 0) <= 0
				then coalesce((seqnb - (lead(seqnb) over win)) + 1, 0)
			else (seqnb - (lead(seqnb) over win) + 4096) + 1
		end) as shift,
		rank,
		name
	FROM
		t
	WINDOW win as (partition by device order by seqnb)
)
,t3 as (
   SELECT
		station as station_transiting_volume,
		count(DISTINCT device) devices_volume,
		count(DISTINCT(device, time)) messages_volume,
		avg(rssi) rssi_average,
		stddev_pop(rssi) rssi_std,
		avg(frames) frame_volume_average,
		avg(redundancy) redundancy_average,
		name as poi,
		case when monarch = 'True' then 1 else 0 end monarch,
		ST_AsGeoJSON(geom_poi) geom,
		(count(*)::double precision/(count(*)::double precision+sum(shift)::double precision)) as "MSR"
	FROM
		t2
	GROUP by
		name,
		monarch,
		station,
		geom_poi
)
SELECT
	*
FROM
	t3
